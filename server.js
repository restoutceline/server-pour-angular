
// Importe le paquet express:
var express = require('express')

var cors = require('cors')


//créé une application express:
var app = express()

const MESSAGES = [
    {
        id: 1,
        title: 'Titre du premier MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      { 
        id: 2,
        title: 'Titre du deuxième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      {
        id: 3,
        title: 'Titre du troisième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      },
      {
        id: 4,
        title: 'Titre du quatrième MessageComponent',
        content: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
        isRead: false,
        sent: new Date()
      }
]


// ws.addlisterner("connect", (r)=>{
//   console.log({
//     message : r
//   });
//   r.addlisterner("message", m => console.log(m))
// });


// app.use(bodyParser.json());
app.use(cors())

app.get("/messages",(req,res)=>{
  // res.setHeader("access-control-allow-origin", "http://localhost:4200/");   ou   app.use(cors())
  res.send(MESSAGES);
})

app.get("/messages/:id", (req,res)=>{
  const m = MESSAGES.find(m => m.id === +req.params.id);

  let status = 200;
  let msg = " super, ça fonctionne";
  if(m === undefined) {
    status = 404;
    msg = " le message n'existe pas dans la zone tampon";
  }

  res.status(status).send({
    message : msg,
    content : m,
    state : status
  });
})

app.post("/messages", (req,res)=>{
  req.body.id = ++lastID;
  MESSAGES.push(req.body);
  res.send(MESSAGES);

})


// Démarrer le serveur et écouter un port donné
app.listen(9933, ()=>{
    console.log(`serveur démarré : http://localhost:9933 `)
})
//web server
